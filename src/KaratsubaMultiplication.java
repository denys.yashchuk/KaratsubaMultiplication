import java.math.BigInteger;

public class KaratsubaMultiplication {

    public static BigInteger multiply(BigInteger x, BigInteger y) {
        BigInteger a, b, c, d, ac, bd, ad_bc, res;
        int numLength = x.toString().length() > y.toString().length() ?
                x.toString().length() : y.toString().length();
        if (numLength < 10)
            return x.multiply(y);
        numLength = numLength / 2 + numLength % 2;
        ad_bc = new BigInteger("10").pow(numLength);
        b = x.divide(ad_bc);
        a = x.subtract(b.multiply(ad_bc));
        d = y.divide(ad_bc);
        c = y.subtract(d.multiply(ad_bc));
        ac = multiply(a, c);
        bd = multiply(b, d);
        ad_bc = multiply(a.add(b), c.add(d));
        res = bd.multiply(new BigInteger("10").pow(numLength * 2));
        res = res.add(ac);
        res = res.add(ad_bc.subtract(ac.add(bd)).multiply(new BigInteger("10").pow(numLength)));
        return res;
    }

}
