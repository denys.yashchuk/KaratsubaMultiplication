import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {
        System.out.println(KaratsubaMultiplication.multiply(
                new BigInteger("8711129198194917883527844183686122989894424943636426448417394566"),
                new BigInteger("2924825637132661199799711722273977411715641477832758942277358764")
        ));
        BigInteger a = new BigInteger("8711129198194917883527844183686122989894424943636426448417394566");
        System.out.println(a.multiply(new BigInteger("2924825637132661199799711722273977411715641477832758942277358764")));
    }
}
